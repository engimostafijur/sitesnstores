<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sitesnstores');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7cL{J]</O<x=nUetQT@dx&q@7r>7[hIFz,a]Y!`e/G&6B< gWmB@]`GNE=eh<nDJ');
define('SECURE_AUTH_KEY',  '}:0N+n^Su[$&Pdw;`6vwZTDdp:@J)Rs-bOOgzWs2YYb+~$T}sg#D>}nCsPD0^z#k');
define('LOGGED_IN_KEY',    'G*^D6ag5>wJeG,kg0QXJ=%W!b![%>#W1iV.eJ#~b>Lw_}r`| |st; -R^vJ@h`LX');
define('NONCE_KEY',        'O;SkKB]s=Y&jYxCaZ9*SMm?slyY[-QNQvAc`6X{TKIoW+ZWj0Ut9pn^/`hU7.gW4');
define('AUTH_SALT',        '>[#syfkY6v1D<5v UDT>d%VpD~9te4~dOFGF1d8G:V eW9nBR$-l+a_6X1Xtj7M>');
define('SECURE_AUTH_SALT', '0M>%rH+I/AMTEbA8VC`NWv?:GU/8q9,.3hdM(, <j%v>n<$yK5xwfH*n}<EE/?ak');
define('LOGGED_IN_SALT',   '+,zK~=w[c#XIRV0wE^=kBw5V6kxhu679LC?vG&=VT-b@!ob9z9/dbjg`>Ldj3/S>');
define('NONCE_SALT',       '|c{5NQR^.jV%z<9CMKr!.#tJF`[NU#M#pp|Ji&VM2p)xCfM-##D=Wj^N0fq ,SLE');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sites_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
