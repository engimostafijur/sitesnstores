$(document).ready(function(){
  $('#owl-carousel').owlCarousel({
    navContainer: '#customNav',
    // move dotsContainer outside the primary owl wrapper
    dotsContainer: '#customDots',
    loop:true,
    margin:10,
    nav:true,
    autoPlay: 2000,
    items : 2, 
     navigationText: [
        "<i class='fa fa-chevron-left'></i>",
        "<i class='fa fa-chevron-right'></i>"
        ]  ,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:2
        },
        1200:{
            items:2
        },
        1900:{
            items:2
        } ,
        2500:{
            items:2
        }
    }  
});

$("#owl-clients").owlCarousel({
    autoPlay: 3000,
    items :6, // THIS IS IMPORTANT
    loop: true,
    slideSpeed : 500,
    rewindSpeed : 500,
    navigation: true,
    nav : true,
    navigationText: [
      "<span class='fa fa-chevron-circle-left'></span>",
      "<span class='fa fa-chevron-circle-right'></span>"
      ],
    pagination:false,
      responsive : {
            320 : { items : 1  },
            480 : { items : 2  }, // from zero to 480 screen width 4 items
            768 : { items : 3  }, // from 480 screen widthto 768 6 items
            1024 : { items : 6   // from 768 screen width to 1024 8 items
            }
        },
         
  });

var header = $(".navbar-fixed-top");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 180) {
            header.removeClass('navbar-fixed-top').addClass("topnavsticky");
        } else {
            header.removeClass("topnavsticky").addClass('navbar-fixed-top');
        }
    });


});

$(document).ready(function() {

$("#owl-confidence").owlCarousel({
    autoPlay: 3000,
    items :1, // THIS IS IMPORTANT
    loop: true,
    navigation: true,
    nav : true,
    navigationText: [
      "<span class='fa fa-chevron-circle-left'></span>",
      "<span class='fa fa-chevron-circle-right'></span>"
      ],    
  });
});

jQuery(function($) {
$('.navbar .dropdown').hover(function() {
$(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

}, function() {
$(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

});

$('.navbar .dropdown > a').click(function(){
location.href = this.href;
});

});



